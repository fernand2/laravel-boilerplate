<?php

namespace App\Support\Enum;

class UserStatus
{
    public const UNCONFIRMED = 'unconfirmed';
    public const ACTIVE = 'active';
    public const BANNED = 'banned';

    public static function lists()
    {
        return [
            self::ACTIVE => trans('app.status.' . self::ACTIVE),
            self::BANNED => trans('app.status.' . self::BANNED),
            self::UNCONFIRMED => trans('app.status.' . self::UNCONFIRMED),
        ];
    }
}
