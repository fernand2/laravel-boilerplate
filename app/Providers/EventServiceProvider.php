<?php

namespace App\Providers;

use App\Events\User\Banned;
use App\Events\User\LoggedIn;
use App\Listeners\Login\UpdateLastLoginTimestamp;
use App\Listeners\Registration\SendSignUpNotification;
use App\Listeners\Users\ActivateUser;
use App\Listeners\Users\InvalidateSessions;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            SendSignUpNotification::class,
        ],
        LoggedIn::class => [
            UpdateLastLoginTimestamp::class,
        ],
        Banned::class => [
            InvalidateSessions::class,
        ],
        Verified::class => [
            ActivateUser::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
