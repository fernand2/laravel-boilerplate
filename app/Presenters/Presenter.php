<?php

namespace App\Presenters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class Presenter
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param $property
     */
    public function __isset(string $property): bool
    {
        return method_exists($this, Str::camel($property));
    }

    public function __get(string $property)
    {
        $camel_property = Str::camel($property);

        if (method_exists($this, $camel_property)) {
            return $this->{$camel_property}();
        }

        return $this->model->{Str::snake($property)};
    }
}
