<?php

namespace App\Http\Controllers\Auth\Api\Auth;

use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\Role\RoleRepository;
use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use Illuminate\Auth\Events\Registered;

class RegistrationController extends ApiController
{
    private UserRepository $users;

    private RoleRepository $roles;

    /**
     * Create a new authentication controller instance.
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(RegisterRequest $request)
    {
        $role = $this->roles->findByName('User');

        $user = $this->users->create(
            array_merge($request->validFormData(), ['role_id' => $role])
        );

        event(new Registered($user));

        return $this->setStatusCode(201)
            ->respondWithArray([
                'requires_email_confirmation' => (bool) setting('reg_email_confirmation'),
            ]);
    }

    /**
     * Verify email via email confirmation token.
     *
     * @param $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyEmail($token)
    {
        if (!setting('reg_email_confirmation')) {
            return $this->errorNotFound();
        }

        if ($user = $this->users->findByConfirmationToken($token)) {
            $this->users->update($user->id, [
                'status' => UserStatus::ACTIVE,
                'confirmation_token' => null,
            ]);

            return $this->respondWithSuccess();
        }

        return $this->setStatusCode(400)
            ->respondWithError('Invalid confirmation token.');
    }
}
