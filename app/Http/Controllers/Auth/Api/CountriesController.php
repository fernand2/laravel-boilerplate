<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Resources\CountryResource;
use App\Repositories\Country\CountryRepository;

class CountriesController extends ApiController
{
    private CountryRepository $countries;

    public function __construct(CountryRepository $countries)
    {
        $this->countries = $countries;
    }

    /**
     * Get list of all available countries.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CountryResource::collection($this->countries->all());
    }
}
