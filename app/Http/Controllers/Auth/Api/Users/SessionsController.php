<?php

namespace App\Http\Controllers\Auth\Api\Users;

use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Resources\SessionResource;
use App\Models\User;
use App\Repositories\Session\SessionRepository;

class SessionsController extends ApiController
{
    public function __construct()
    {
        $this->middleware('permission:users.manage');
        $this->middleware('session.database');
    }

    /**
     * Get sessions for specified user.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(User $user, SessionRepository $sessions)
    {
        return SessionResource::collection(
            $sessions->getUserSessions($user->id)
        );
    }
}
