<?php

namespace App\Http\Controllers\Auth\Api\Profile;

use App\Events\User\UpdatedProfileDetails;
use App\Http\Controllers\Auth\Api\ApiController;
use App\Http\Requests\User\UpdateProfileDetailsRequest;
use App\Http\Resources\UserResource;
use App\Repositories\User\UserRepository;

class DetailsController extends ApiController
{
    /**
     * Handle user details request.
     *
     * @return UserResource
     */
    public function index()
    {
        return new UserResource(auth()->user());
    }

    /**
     * Updates user profile details.
     *
     * @return UserResource
     */
    public function update(UpdateProfileDetailsRequest $request, UserRepository $users)
    {
        $user = $request->user();

        $data = collect($request->all());

        $data = $data->only([
            'first_name', 'last_name', 'birthday',
            'phone', 'address', 'country_id',
        ])->toArray();

        if (!isset($data['country_id'])) {
            $data['country_id'] = $user->country_id;
        }

        $user = $users->update($user->id, $data);

        event(new UpdatedProfileDetails());

        return new UserResource($user);
    }
}
