<?php

namespace App\Http\Controllers\Auth\Web\Auth;

use App\Events\User\LoggedIn;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;
use App\Services\Auth\ThrottlesLogins;
use App\Services\Auth\TwoFactor\Authy;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TwoFactorTokenController extends Controller
{
    use ThrottlesLogins;

    private UserRepository $users;

    /**
     * Create a new authentication controller instance.
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Show Two-Factor Token form.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show()
    {
        return session('auth.2fa.id') ? view('auth.token') : redirect('login');
    }

    /**
     * Handle Two-Factor token form submission.
     *
     * @throws \Illuminate\Validation\ValidationException
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        if (!session('auth.2fa.id')) {
            return redirect('login');
        }

        $user = $this->users->find(
            $request->session()->pull('auth.2fa.id')
        );

        if (!$user) {
            throw new NotFoundHttpException();
        }

        if (!Authy::tokenIsValid($user, $request->token)) {
            return redirect()->to('login')
                ->withErrors(__('2FA Token is invalid!'));
        }

        Auth::login($user);

        event(new LoggedIn());

        return redirect()->intended('/');
    }
}
