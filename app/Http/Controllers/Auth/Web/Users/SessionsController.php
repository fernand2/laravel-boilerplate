<?php

namespace App\Http\Controllers\Auth\Web\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Session\SessionRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class SessionsController.
 */
class SessionsController extends Controller
{
    private SessionRepository $sessions;

    /**
     * SessionsController constructor.
     */
    public function __construct(SessionRepository $sessions)
    {
        $this->middleware('permission:users.manage');

        $this->sessions = $sessions;
    }

    /**
     * Displays the list with all active sessions for the selected user.
     *
     * @return Factory|View
     */
    public function index(User $user)
    {
        return view('user.sessions', [
            'adminView' => true,
            'user' => $user,
            'sessions' => $this->sessions->getUserSessions($user->id),
        ]);
    }

    /**
     * Invalidate specified session for selected user.
     *
     * @param $session
     */
    public function destroy(User $user, $session)
    {
        $this->sessions->invalidateSession($session->id);

        return redirect()->route('users.sessions', $user->id)
            ->withSuccess(__('Session invalidated successfully.'));
    }
}
