<?php

namespace App\Http\Controllers\Auth\Web\Users;

use App\Events\User\Banned;
use App\Events\User\UpdatedByAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateDetailsRequest;
use App\Models\User;
use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use Illuminate\Http\Request;

/**
 * Class UserDetailsController.
 */
class DetailsController extends Controller
{
    private UserRepository $users;

    /**
     * UsersController constructor.
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Updates user details.
     */
    public function update(User $user, UpdateDetailsRequest $request)
    {
        $data = $request->all();

        if (!data_get($data, 'country_id')) {
            $data['country_id'] = null;
        }

        $this->users->update($user->id, $data);
        $this->users->setRole($user->id, $request->role_id);

        event(new UpdatedByAdmin($user));

        // If user status was updated to "Banned",
        // fire the appropriate event.
        if ($this->userWasBanned($user, $request)) {
            event(new Banned($user));
        }

        return redirect()->back()
            ->withSuccess(__('User updated successfully.'));
    }

    /**
     * Check if user is banned during last update.
     *
     * @return bool
     */
    private function userWasBanned(User $user, Request $request)
    {
        return $user->status != $request->status
            && UserStatus::BANNED == $request->status;
    }
}
