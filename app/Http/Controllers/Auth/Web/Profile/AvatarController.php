<?php

namespace App\Http\Controllers\Auth\Web\Profile;

use App\Events\User\ChangedAvatar;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;
use App\Services\Upload\UserAvatarManager;
use Illuminate\Http\Request;

/**
 * Class ProfileController.
 */
class AvatarController extends Controller
{
    private UserRepository $users;

    /**
     * UsersController constructor.
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Upload and update user's avatar.
     */
    public function update(Request $request, UserAvatarManager $avatarManager)
    {
        $request->validate(['avatar' => 'image']);

        $name = $avatarManager->uploadAndCropAvatar(
            $request->file('avatar'),
            $request->get('points')
        );

        if ($name) {
            return $this->handleAvatarUpdate($name);
        }

        return redirect()->route('profile')
            ->withErrors(__('Avatar image cannot be updated. Please try again.'));
    }

    /**
     * Update user's avatar from external location/url.
     */
    public function updateExternal(Request $request, UserAvatarManager $avatarManager)
    {
        $avatarManager->deleteAvatarIfUploaded(auth()->user());

        return $this->handleAvatarUpdate($request->get('url'));
    }

    /**
     * Update avatar for currently logged in user
     * and fire appropriate event.
     *
     * @param $avatar
     */
    private function handleAvatarUpdate($avatar)
    {
        $this->users->update(auth()->id(), ['avatar' => $avatar]);

        event(new ChangedAvatar());

        return redirect()->route('profile')
            ->withSuccess(__('Avatar changed successfully.'));
    }
}
