<?php

namespace App\Http\Controllers\Auth\Web\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateProfileLoginDetailsRequest;
use App\Repositories\User\UserRepository;

/**
 * Class LoginDetailsController.
 */
class LoginDetailsController extends Controller
{
    private UserRepository $users;

    /**
     * LoginDetailsController constructor.
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Update user's login details.
     */
    public function update(UpdateProfileLoginDetailsRequest $request)
    {
        $data = $request->except('role', 'status');

        // If password is not provided, then we will
        // just remove it from $data array and do not change it
        if (!data_get($data, 'password')) {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $this->users->update(auth()->id(), $data);

        return redirect()->route('profile')
            ->withSuccess(__('Login details updated successfully.'));
    }
}
