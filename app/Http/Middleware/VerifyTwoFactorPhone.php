<?php

namespace App\Http\Middleware;

use App\Repositories\User\UserRepository;
use Closure;

class VerifyTwoFactorPhone
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle($request, Closure $next)
    {
        $user = $this->getUser($request);

        if ($user->two_factor_country_code && $user->two_factor_phone) {
            return $next($request);
        }

        abort(404);
    }

    /**
     * @param $request
     */
    private function getUser($request)
    {
        if ($userId = $request->get('user')) {
            return app(UserRepository::class)->find($userId);
        }

        return $request->user();
    }
}
