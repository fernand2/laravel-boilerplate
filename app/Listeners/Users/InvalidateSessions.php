<?php

namespace App\Listeners\Users;

use App\Events\User\Banned;
use App\Repositories\Session\SessionRepository;

class InvalidateSessions
{
    private SessionRepository $sessions;

    public function __construct(SessionRepository $sessions)
    {
        $this->sessions = $sessions;
    }

    /**
     * Handle the event.
     */
    public function handle(Banned $event): void
    {
        $user = $event->getBannedUser();

        $this->sessions->invalidateAllSessionsForUser($user->id);
    }
}
