<?php

namespace App\Listeners\Users;

use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use Illuminate\Auth\Events\Verified;

class ActivateUser
{
    private UserRepository $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Handle the event.
     */
    public function handle(Verified $event): void
    {
        $this->users->update($event->user->id, [
            'status' => UserStatus::ACTIVE,
        ]);
    }
}
