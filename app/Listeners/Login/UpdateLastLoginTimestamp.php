<?php

namespace App\Listeners\Login;

use App\Events\User\LoggedIn;
use App\Repositories\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;

class UpdateLastLoginTimestamp
{
    private UserRepository $users;

    private Guard $guard;

    public function __construct(UserRepository $users, Guard $guard)
    {
        $this->users = $users;
        $this->guard = $guard;
    }

    /**
     * Handle the event.
     */
    public function handle(LoggedIn $event): void
    {
        $this->users->update(
            $this->guard->id(),
            ['last_login' => Carbon::now()]
        );
    }
}
