<?php

namespace App\Listeners\Registration;

use App\Mail\UserRegistered;
use App\Repositories\User\UserRepository;
use Illuminate\Auth\Events\Registered;
use Mail;

class SendSignUpNotification
{
    private UserRepository $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Handle the event.
     */
    public function handle(Registered $event): void
    {
        // todo: if (!setting('notifications_signup_email')) return;

        foreach ($this->users->getUsersWithRole('Admin') as $user) {
            Mail::to($user)->send(new UserRegistered($event->user));
        }
    }
}
