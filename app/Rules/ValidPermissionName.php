<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidPermissionName implements Rule
{
    protected $regex = '/^[a-zA-Z0-9\-_\.]+$/';

    public function __toString(): string
    {
        return sprintf('regex:%s', $this->regex);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!\is_string($value) && !is_numeric($value)) {
            return false;
        }

        return preg_match($this->regex, $value) > 0;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return __('validation.regex', ['attribute' => __('permission name')]);
    }
}
