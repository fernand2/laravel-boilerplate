<?php

namespace App\Repositories\Role;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

interface RoleRepository
{
    /**
     * Get all system roles.
     */
    public function all(): Collection;

    /**
     * Lists all system roles into $key => $column value pairs.
     */
    public function lists(string $column = 'name', string $key = 'id');

    /**
     * Get all system roles with number of users for each role.
     */
    public function getAllWithUsersCount();

    /**
     * Find system role by id.
     *
     * @param $id Role Id
     */
    public function find($id): ?Role;

    /**
     * Find role by name:.
     *
     * @param $name
     */
    public function findByName(string $name): Role;

    /**
     * Create new system role.
     */
    public function create(array $data): Role;

    /**
     * Update specified role.
     *
     * @param $id Role Id
     */
    public function update($id, array $data): Role;

    /**
     * Remove role from repository.
     *
     * @param $id Role Id
     */
    public function delete($id): bool;

    /**
     * Update the permissions for given role.
     *
     * @param $roleId
     */
    public function updatePermissions($roleId, array $permissions);
}
