<?php

namespace App\Repositories\Country;

interface CountryRepository
{
    /**
     * Create $key => $value array for all available countries.
     *
     * @param string $column
     * @param string $key
     */
    public function lists($column = 'name', $key = 'id');

    /**
     * Get all available countries.
     */
    public function all();
}
