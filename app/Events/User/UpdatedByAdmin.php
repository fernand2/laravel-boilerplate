<?php

namespace App\Events\User;

use App\Models\User;

class UpdatedByAdmin
{
    protected User $updatedUser;

    public function __construct(User $updatedUser)
    {
        $this->updatedUser = $updatedUser;
    }

    public function getUpdatedUser(): User
    {
        return $this->updatedUser;
    }
}
